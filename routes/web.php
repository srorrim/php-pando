<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Faker\Factory;
use Faker\Provider\DateTime;
use Faker\Provider\Image;
use Faker\Provider\Lorem;
Route::get('/about', function () {
    return view('about');
});

class Featured {}
class Articles {}

Route::get('/', function () {
    $faker = Factory::create();

  $featured = new Featured();
  $featured->week = $faker->dayOfWeek($max = 'now');
  $featured->month = $faker->monthName($max = 'now');
  $featured->day = $faker->dayOfMonth($max = 'now');
  $featured->year = $faker->year($max = 'now');
  $featured->title = $faker->sentence($nbWords = 4, $variableNbWords = true);
  $featured->quote = $faker->text($maxNbChars = 300);
  $featured->image = $faker->imageUrl($width = 525, $height = 650);
  $featured->pimage = $faker->imageUrl($width = 30, $height = 30);
  $featured->name = $faker->name;


  $articles = new Articles();
  $articles->title = $faker->sentence($nbWords = 6, $variableNbWords = true);
  $articles->quote = $faker->text($maxNbChars = 200);
  $articles->name = $faker->name;
  $articles->image = $faker->imageUrl($width = 250, $height = 170);
  $articles->pimage = $faker->imageUrl($width = 30, $height = 30);



  $data = [
          'featured' => $featured,
          'articles' => $articles
      ];

      return view('welcome', $data);
});
