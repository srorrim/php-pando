<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/png" href=""/>
        <link rel="stylesheet" href="/css/app.css">
        <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <title>PhP'izzzle m'nizzle</title>
    </head>
    <body>
        <div id='header'>
          <hr>
          <ul>
              <a href="https://pando.com/"><img class="logo" src="https://assets.pando.com/img/logo.2f6433f269a0.svg" alt=""/></a>
              <a href="https://pando.com/archives/"><li>Archive</li></a>
              <a href="https://pando.com/events/"><li>Interviews</li></a>
              <a href="https://pando.com/subscribe/"><li>Join</li></a>
              <a href="https://pando.com/member/login/"><li>Login</li></a>
              <a href="https://pando.com/search/"><li>Search</li></a>
          </ul>
       </div>


        @yield('content')
        @yield('footer')
    </body>
</html>
