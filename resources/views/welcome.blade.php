@extends('layout')
@section('content')
<section id='featured-container'>
  <div id="featuredL">

    <div id="featuredTop">
        <a href="https://pando.com/subscribe/"><img src="../img/Capture.PNG" alt="P logo"/></a>
         <div id="fDate">
           <h4><?php echo $featured->week ?> <?php echo $featured->month ?> <?php echo $featured->day ?> <?php echo $featured->year ?></h4>
          <h5>Featured Story</h5>
        </div>
    </div>

    <div id="featured-bottom">
      <h1 id="featured"><?php echo $featured->title ?></h1>
      <p><?php echo $featured->quote ?></p>
      <div id="author1">
        <img src='<?php echo $featured->pimage ?>' alt=""/><p>By: </p><h3><?php echo $featured->name ?></h3>
      </div>
    </div>
  </div>

  <div id="featuredR">
      <img src="<?php echo $featured->image ?>" alt="featured image">
  </div>
  </section>
  <section id='articles-container'>
      <div id='dateList'>
          <ul>
            <p>Previous Articles:</p>
            <li><a href="" onclick="Articles();"><?php $articles ?>Feburary,<?php echo $featured->day ?>,<?php echo $featured->year ?></a></li>
            <li><a href="" onclick="Articles();"><?php $articles ?>Feburary,<?php echo $featured->day ?>,<?php echo $featured->year ?></a></li>
            <li><a href="" onclick="Articles();"><?php $articles ?>Feburary,<?php echo $featured->day ?>,<?php echo $featured->year ?></a></li>
            <li><a href="" onclick="Articles();"><?php $articles ?>Feburary,<?php echo $featured->day ?>,<?php echo $featured->year ?></a></li>
        </ul>
      </div>

      <div id="articles">
          <img src='<?php echo $articles->image ?>' alt=""/>
            <span>
               <h1><?php echo $articles->title ?><h1>
               <p><?php echo $articles->quote ?></p>
            </span>
            <div id="author2">
                <img src='<?php echo $articles->pimage ?>' alt=""/><p>By:</p><h3><?php echo $articles->name?></h3>
            </div>
      </div>
  </section>

  <section id="archives">
      <h1>Want more to read? <a href="https://pando.com/archives/">Visit the archives.</a></h1>
  </section>

  <section id='sub-footer'>
      <a href="https://pando.com/subscribe/"><img src="../img/sub-footer.PNG" alt="sub_footer_image"/></a>
  </section>

  <footer>
      <div id="footer">
            <img id="footer-logo" src="https://assets.pando.com/img/logo.2f6433f269a0.svg" alt=""/>
            <h6>@Copyright 2018 PandoMedia Inc.</h6>
      </div>

      <div id="footer-links">
        <h3>About/Disclosures</h3>
        <h3>Contact Us</h3>
        <h3>@pandodaily</h3>
        <h3>Facebook</h3>
      </div>
  </footer>

@endsection
