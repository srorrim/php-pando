<?php

use Illuminate\Database\Seeder;
use App\Models\Tweet;
use Faker\Factory;
use Faker\Provider\Lorem;
use Faker\Provider\en_US\Company;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $tweet = new Tweet();
        $tweet->content = $faker->catchPhrase;
        $tweet->handle = $faker->lastName;
        $tweet->date = $faker->date($format = 'Y-m-d', $max = 'now');
        $tweet->save();

    }
}
